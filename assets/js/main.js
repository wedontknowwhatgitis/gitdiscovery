$('#toggleRegister').on('click', function(e){
    e.preventDefault();

    $('.login-panel').hide();
    $('.register-panel').fadeIn().addClass('magictime slideDownReturn');

    setTimeout(function(){
        $('.register-panel').removeClass('magictime slideDownReturn');
    }, 1000);

});

$('#toggleLogin').on('click', function(e){
    e.preventDefault();

    $('.register-panel').hide();
    $('.login-panel').fadeIn().addClass('magictime slideDownReturn');

    setTimeout(function(){
        $('.login-panel').removeClass('magictime slideDownReturn');
    }, 1000);
    
});


$(".slider-container").ikSlider({
  speed: 500
});
$(".slider-container").on('changeSlide.ikSlider', function (evt) { console.log(evt.currentSlide); });


AOS.init();


$(document).ready(function() {
  
  $(window).scroll(function () {
      
    if ($(window).scrollTop() > 80) {
      $('.navbar').addClass('navbar-fixed');
      
      $('.top-bar').hide();
    }
    if ($(window).scrollTop() < 80) {
      $('.navbar').removeClass('navbar-fixed');
      $('.top-bar').fadeIn();

    }



    
  });
});